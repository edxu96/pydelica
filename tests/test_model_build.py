import logging
import os
import platform

import pytest

from pydelica.compiler import Compiler

logging.getLogger("PyDelica").setLevel(logging.DEBUG)


@pytest.fixture(scope="module")
def compile_model_custom(modelica_environment):
    _sfode_src = os.path.join(os.path.dirname(__file__), "models", "SineCurrentMSL3.mo")
    _compiler = Compiler()
    return _compiler.compile(_sfode_src, custom_library_spec=modelica_environment)


@pytest.fixture(scope="module")
def compile_model_default():
    _sfode_src = os.path.join(os.path.dirname(__file__), "models", "SineCurrentMSL4.mo")
    _compiler = Compiler()
    return _compiler.compile(_sfode_src)


@pytest.mark.compilation
def test_binary_msl3(compile_model_custom):
    assert os.path.exists(compile_model_custom)


@pytest.mark.compilation
def test_binary_msl4(compile_model_default):
    assert os.path.exists(
        os.path.join(compile_model_default)
    )
