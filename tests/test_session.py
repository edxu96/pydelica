import os
import pprint

import pytest

import pydelica.exception as pde
from pydelica import Session
from pydelica.logger import OMLogLevel
from pydelica.model import Model


@pytest.fixture
def pydelica_session():
    _sfode_src = os.path.join(os.path.dirname(__file__), "models", "SineCurrentMSL4.mo")
    with Session(log_level=OMLogLevel.DEBUG) as _session:
        _session.build_model(_sfode_src)
        yield _session


@pytest.mark.session
def test_build_with_c_source(pydelica_session: Session):
    _util_src = os.path.join(os.path.dirname(__file__), "models", "Utilities.mo")
    _c_source_dir = os.path.join(os.path.dirname(__file__), "c_sources")
    pydelica_session.build_model(
        _util_src, c_source_dir=_c_source_dir, model_addr="Utilities.test"
    )


@pytest.mark.session
def test_get_binary_loc(pydelica_session: Session):
    assert pydelica_session.get_binary_location("SineCurrentModel")


@pytest.mark.session
def test_get_parameters(pydelica_session: Session):
    _params = pydelica_session.get_parameters()
    pprint.pprint(_params)
    assert _params


@pytest.mark.session
def test_get_single_parameter(pydelica_session: Session):
    assert pydelica_session.get_parameter("resistor.T_ref") == 300.15


@pytest.mark.session
def test_get_simopts(pydelica_session: Session):
    _opts = pydelica_session.get_simulation_options()
    pprint.pprint(_opts._opts)
    assert _opts._opts


@pytest.mark.session
def test_get_option(pydelica_session: Session):
    assert pydelica_session.get_simulation_option("solver") == "dassl"


@pytest.mark.session
def test_set_parameter(pydelica_session: Session):
    pydelica_session.set_parameter("resistor.alpha", 0.2)
    pydelica_session.set_parameter("sineCurrent.I", 0.0)
    pydelica_session._model_parameters["SineCurrentModel"].write_params()
    with Session() as _other_session:
        _xml = pydelica_session._model_parameters["SineCurrentModel"]._model_xml
        _other_session._model_parameters["SineCurrentModel"] = Model("", _xml)
        assert _other_session.get_parameter("resistor.alpha") == 0.2
        assert _other_session.get_parameter("sineCurrent.I") == 0.0


@pytest.mark.session
def test_simulation(pydelica_session: Session):
    pydelica_session.simulate()
    assert pydelica_session._solutions["SineCurrentModel"]


@pytest.mark.session
def test_terminate_on_assertion_never(pydelica_session: Session):
    pydelica_session.fail_on_assert_level("never")
    pydelica_session.set_parameter("resistor.alpha", 0)
    pydelica_session.simulate()
    pydelica_session.set_parameter("resistor.alpha", 0.1)
    pydelica_session.fail_on_assert_level("error")


@pytest.mark.session
def test_terminate_on_assertion_error(pydelica_session: Session):
    pydelica_session.set_parameter("resistor.alpha", -1)
    with pytest.raises(pde.OMAssertionError):
        pydelica_session.simulate()
    pydelica_session.set_parameter("resistor.alpha", 1.2)
    pydelica_session.simulate()
    pydelica_session.set_parameter("resistor.alpha", 0.1)


@pytest.mark.session
def test_terminate_on_assertion_warning(pydelica_session: Session):
    pydelica_session.fail_on_assert_level("warning")
    pydelica_session.set_parameter("resistor.alpha", 1.2)
    with pytest.raises(pde.OMAssertionError):
        pydelica_session.simulate()
    pydelica_session.set_parameter("resistor.alpha", 0.1)
    pydelica_session.fail_on_assert_level("error")
